import { types, Category, Product } from "./types";

interface State {
  category?: Category[]
  product?: Product[]
  loading: boolean
}
const initialState: State = {
  loading: false,
  category: []
}
const productReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case types.LOAD_CATEGORY:
      return {
        ...state,
        loading: true
      }
    case types.LOAD_CATEGORY_SUCCESS:
      return {
        ...state,
        category: [...action.payload],
        loading: false
      }
    default:
      return state
  }
}
export default productReducer
