const LOAD_ORDERS_REQUEST: string = 'LOAD_ORDERS_REQUEST'
const LOAD_ORDERS_SUCCESS: string = 'LOAD_ORDERS_SUCCESS'
const CLICK_ORDER_REQUEST: string = 'CLICK_ORDER_REQUEST'
const CLICK_REORDER_REQUEST: string = 'CLICK_REORDER_REQUEST'
const CHECK_OUT_CONFIG_REQUEST: string = 'CHECK_OUT_CONFIG_REQUEST'
const CHECK_OUT_CONFIG_SUCCESS: string = 'CHECK_OUT_CONFIG_REQUEST_SUCCESS'
const CHECK_OUT_CONFIG_FAILURE: string = 'CHECK_OUT_CONFIG_FAILURE'
const GET_PAYMENT_METHODS_REQUEST: string = 'GET_PAYMENT_METHODS_REQUEST'
const GET_PAYMENT_METHODS_SUCCESS: string = 'GET_PAYMENT_METHODS_SUCCESS'
const GET_PAYMENT_METHODS_FAILURE: string = 'GET_PAYMENT_METHODS_FAILURE'

export const types = {
  LOAD_ORDERS_REQUEST,
  LOAD_ORDERS_SUCCESS,
  CLICK_ORDER_REQUEST,
  CLICK_REORDER_REQUEST,
  CHECK_OUT_CONFIG_REQUEST,
  CHECK_OUT_CONFIG_SUCCESS,
  CHECK_OUT_CONFIG_FAILURE,
  GET_PAYMENT_METHODS_REQUEST,
  GET_PAYMENT_METHODS_SUCCESS,
  GET_PAYMENT_METHODS_FAILURE,
}
export interface OrdersState {
  address?: string
  currency?: string
  customerName?: string
  email?: string
  id?: number
  items?: Product[]
  orderStatus?: string
  orderType?: string
  paymentId?: number
  paymentStatus?: string
  phoneNumber?: number
  qtableId?: number
  storeId?: number
  tableNumber?: number
  totalAmount?: number
  orders?: any
  orderClick?: any
  reOrder: any
  CheckOutConfig?: any
  PaymentMethods?: any
}

export interface Product {
  id: number
  categoryId: number
  itemName: string
  price: number
  taxRateIfPickUp: number
  taxRateIfDineIn: number
  information: string
  ingredient: string
  image: string
  imageContentType: string
  itemOptions: ItemOption[]
  itemRatings?: any
  from: string
  loading?: boolean
  category?: any

}
export interface ItemOption {
  id: number
  name: string
  choice: string
  price: number
  itemOptionGroups: ItemOptionGroup[]
  item: object[]
}

export interface ItemOptionGroup {
  id: number
  name: string
  choice: string
  price: number
  optionName: string
  activeOptionName: string
}


export interface LoadOrdersAction {
  type: typeof LOAD_ORDERS_REQUEST
}

export interface ClickOrderRequest {
  type: typeof CLICK_ORDER_REQUEST
  payload: ItemOption
}

export interface ReClickOrderRequest {
  type: typeof CLICK_REORDER_REQUEST
  payload: ItemOption
}

export type UserActionTypes = LoadOrdersAction | ClickOrderRequest | ReClickOrderRequest