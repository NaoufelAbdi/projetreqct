import axios from 'axios';

const Base_URL = "https://kwicksit.herokuapp.com/api"

export function callLogin(action: any) {
    return axios.post(`${Base_URL}/authenticate`, action.payload);
}

export function callRegister(action: any) {
    return axios.post(`${Base_URL}/register`, action.payload);
}

export function callUserInfo(token: any) {
    return axios.get(`${Base_URL}/account`,  { headers: {"Authorization" : `Bearer ${token}`} });
}

export function callUserUpdate( action: any ,token: any) {
    return axios.post(`${Base_URL}/account`, action.payload, { headers: {"Authorization" : `Bearer ${token}`} });
}

export function callPasswordChange(action: any , token: any) {
    return axios.post(`${Base_URL}/account/change-password`, action.payload, { headers: {"Authorization" : `Bearer ${token}`} });
}