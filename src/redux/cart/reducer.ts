import { Cart, types, } from './types'

const initialState: Cart = {
  cart: [],
  grandTotal: 0,
  loading: false,
  orderResponse: "",
  isOrderSubmit: false
}

const cartReducer = (state = initialState, action: any) => {
  let { cart } = state
  switch (action.type) {
    case types.ADD_PRODUCT_TO_CART:
      cart.push({ id: Date.now(), ...action.payload })
      return {
        ...state,
        cart,
      }
    case types.UPDATE_PRODUCT_IN_CART:
      for (let i = 0; i < cart.length; i++) {
        if (cart[i].id === action.payload.id) {
          cart[i] = action.payload
          break
        }
      }
      return {
        ...state,
        cart,
      }
    case types.REMOVE_PRODUCT_FROM_CART:
      let cartID = action.payload
      cart = cart.filter(cartItem => cartItem.id !== cartID)
      return {
        ...state,
        cart,
      }
    case types.GRAND_TOTAL_SAVE:
      return {
        ...state,
        grandTotal: action.payload
      }
    case types.ORDER_DATA_REQUEST:
      return {
        ...state,
        loading: true,
        isOrderSubmit: false
      }
    case types.ORDER_DATA_SUCCESS:
      return {
        ...state,
        loading: false,
        isOrderSubmit: true,
        orderResponse: action.payload
      }
    case types.START_NEW_ORDER:
      return {
        ...state,
        grandTotal: 0,
        cart: [],
      }
    default:
      return state
  }
}

export default cartReducer
