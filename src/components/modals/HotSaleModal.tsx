import React, { Component } from 'react'
import { WithTranslation } from 'react-i18next'

interface Props {
  onClose?(): void

  visible: boolean
}

class HotSaleModal extends React.Component<Props> {
  render(): React.ReactNode {
    const {
      onClose,
      visible,
    } = this.props
    return (
      <div className={`modal ${visible ? 'open' : ''}`}>
        <div className={'hotsale-card'}>
          <div className={'hotSale-header'}>
            <h3 className={'title'}>HOT SALE</h3>
          </div>
          <div className={'details'}>
            <p>Get your order in by 12pm today and get a discount with coupon GOLD1321</p>
            <div className={'ok-button'} onClick={onClose}>
              <p>OK</p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default HotSaleModal
