import React from 'react'
import CloseTag from '../CloseTag'
import HorizontalLine from '../HorizontalLine'
import Option from '../../interfaces/Option'
import PlusIcon from '../../assets/icons/plus.png'
import MinusIcon from '../../assets/icons/minus.png'
import VerticalLine from '../VerticalLine'
import { CartItem } from '../../redux/cart/types'
import { Product } from '../../redux/products/types'
import SizeOptionCheckbox from '../SizeOptionCheckbox'
import { channel } from '@redux-saga/core'
import { threadId } from 'worker_threads'

interface Props {
  product?: Product,
  sizesData: Option[]
  selectedOptions: Option[]
  selectedExtraOptions: Option[]
  selectedSize: Option
  onOptionPress(option: Option): void
  onExtraIngredientPress(option: Option): void
  onSizePress(size: Option): void
  onAddItem?(cartItem: CartItem): void
  onClose?(): void
  visible: boolean

}

interface State {
  quantity: number;   /*La quantité du produit*/
  selectedOption: any[], /*Le tableau de type any qui peut reçevoir n'importe quel type d'elements, c'est là on va stocké les produits choisi et additioner les options des produits*/
  totalPrice: number,   /*cette variable est déclarée au sein du props pour qu'on update chaque fois à chaque instanst lors de nouvel choix le pris de notre commande */
  changed: boolean   /*cette varibale de type boolean est appelé lors de la mise à jours effectuer pour chaque appel a notre model c-à-d lors du choix d'un nouveau produit*/
}

class ProductDetailModal extends React.Component<Props, State> {
  state: Readonly<State> = {
    quantity: 1, /*Initialisation de notre variables props */
    selectedOption: [],
    totalPrice: this.props.product?.price || 0,
    changed: false
  }

  componentDidUpdate(prevProp, prevState) {   /*est appelée immédiatement après que la mise à jour a eu lieu*/
    if (prevProp.product?.id != this.props.product?.id) {
      this.setState({ quantity:1, totalPrice: (this.props.product?.price || 0)}); /*alors si il trouve sue les éléments du props sont pas nouvelles il va pas faire une mise à jpurs sinon il le fait */
    }       /*Cela peut ce traduire que à chaque fois qu'un client choisi un nouveau produit cette fonction est exécuter pour initialiser les props */

    if (this.state.changed) {  /*on teste si la mise a jour est faite c-a-d le client a effectué une opération alors on va calculer le pris des élements selectionner  */
      let total = 0;    /*et on redonne à changed= false pour un nouveau choix */
      for (let i = 0; i < this.state.selectedOption.length; i++) {
        total += this.state.selectedOption[i].price;
      }
      this.setState({ changed: false, totalPrice: (this.props.product.price + total)});
    }
  }

  componentWillUnmount() {
    this.setState({ quantity:1, totalPrice: this.props.product?.price || 0 }); /* initialisation de la quantité et prix */
  }

  increaseQuantity = () => {
    this.setState({ quantity: this.state.quantity + 1 }); /*fonction appler lors de l'incrémentation du prix*/
  }

  decreaseQuantity = () => {
    if (this.state.quantity > 1) {  /* le contraire de increaseQuantity*/
      this.setState({ quantity: this.state.quantity - 1 });
    }
  }

  onAddItem = () => {
    const cartItem: CartItem = { /*cette fonction prend en argument une variable de type cartItem et c'est la fonction qui permet d'ajouter*/
      product: this.props.product, /*à la cart des voeux les choix souhaités*/
      quantity: this.state.quantity,
      size: [],
      sizePrice: [],
      totalPrice: this.state.totalPrice
    }
    this.props.onAddItem(cartItem) 
  }

  render(): React.ReactNode {
    const {
      onClose,
      visible,
      product,
    } = this.props;

    const result = {}

    for (let i = 0; i < product?.itemOptions.length; i++) {
      let group = product?.itemOptions[i].itemOptionGroups[0]
      if (!result[group.optionName]) {
        result[group.optionName] = [product?.itemOptions[i]]
      } else {
        result[group.optionName].push(product?.itemOptions[i])
      }
    }
    const groupIndexing = {}
    const merged = []
    for (let i = 0; i < product?.itemOptions.length; i++) {
      const optionGroupName = product.itemOptions[i].itemOptionGroups[0].optionName
      const itemOptions = result[optionGroupName]
      if (groupIndexing[optionGroupName] === undefined) {
        groupIndexing[optionGroupName] = i
        merged.push({
          groupName: optionGroupName,   /*Ce traitement est réalisé avec des condition et boucles pour à la fin mettre dans le tableau Merged */
          itemOptions,              /*les élements optiongroupe name qui représente l'ensemble des options de chaque produit et les options eux meme*/
        })
      }
    }

    let totale = this.state.totalPrice

    return (
      
      <div className={`modal ${visible ? 'open' : ''}`}>
        <div className={'body  md'}>
          <div className={'header ph'}>
            <CloseTag onClose={onClose}/>
            <h3 className={'title'}>{product?.itemName}</h3> {/*on est dans l'interface qui affiche pour chaque produit ses options*/}
            <p className={'sub'}>{product?.information}</p>
          </div>
          
          {merged.map((key) => { {/*on va lister sur Merged ou on a les options et leurs nom avec map*/}
            return (
              <div className={'content'}>
                <h5 className={'group-header'}>{key.groupName}</h5>
                <HorizontalLine/>
                <div className={'product-options'}>
                 {key.itemOptions.map((option) => (
                          <SizeOptionCheckbox 
                            onToggle={(selectedOption) => { /*action a executer le moment du clique*/
                              this.setState({ changed: true })
                              if(selectedOption.id === option.id && !this.state.selectedOption.includes(selectedOption)) { /*je teste ici si l'element choisi par l user est celui que j'ai dans mon tableau */
                                 this.setState({ selectedOption: [...this.state.selectedOption, selectedOption] }) /*selectedoption avec l'id et si il n'est pas dans mon tableau je vais l'insérer*/
                              }
                              else {
                                 this.setState({ selectedOption: [...this.state.selectedOption.filter(op => op.id === selectedOption.id)] }) /*sinon je vais le filter de mon tableuu selected c-a-d */
                              } /*l'enlever pour qu'à la fin j'obtien juste l'lement selectionné pour que j'ajoute juste son prix*/
                            }}
                            option={option} /*et maintenant je vais avoir dans mon tableau selectedoption juste les élements selectionne de user et je fais l'affectation du prix et choix juste de l'option*/
                            /*active={this.state.selectedOption && (this.state.selectedOption.id === option.id)}*/
                            label={option.choice}
                            price={option.price}
                            name={key.groupName}
                          />
                        )
                    )
                 }
                   {/* {key.itemOptions.map((size) => {
                      // const isActive = this.state.selected === size.itemOptionGroups[0].activeOptionName
                      // const isActive = size.itemOptionGroups[0].activeOptionName
                      let isSelected = false
                      for(let i = 0; i <= this.state.selected.length; i++){
                        if(this.state.selected[i] && this.state.selected[i].id === size.id){
                        //if(this.state.selected === size.itemOptionGroups[0].activeOptionName){
                          isSelected = true;
                          break;
                        }  
                      }
                      return (
                        <SizeOptionCheckbox
                          onToggle={(isSelected) => {
                            let foundIndex = -1;
                            for(let i = 0; i <= this.state.selected.length; i++){
                              console.warn(this.state.selected[i])
                              if(this.state.selected[i] && this.state.selected[i].id === size.id){
                                foundIndex = i;
                                break;
                              }
                            }
                            let {selected} = this.state;
                            if(foundIndex > -1) { 
                              selected.splice(foundIndex, 1)
                              
                              
                            } else {
                                selected.push(size)
                                
                            }
                            this.setState({selected})
                          }
                        }
                          option={size}
                          label={size.choice}
                          price={size.price}
                          active={isSelected}/>
                      )
                    } */}
                  
               </div>
               </div>
            )
            }
            )
          } 
          <div className={'footer'}>
            <div className={'left'}>
              <div className={'quantity'}>
                <img
                  src={MinusIcon}
                  alt="Plus Amount"
                  className={'plus-minus-icon'}
                  onClick={this.decreaseQuantity}
                />
                <h3>{this.state.quantity}</h3>
                <img
                  src={PlusIcon}
                  alt="Minus Amount"
                  className={'plus-minus-icon'}
                  onClick={this.increaseQuantity}
                />
              </div>
              <VerticalLine/>
              <div className={'price'}>
                <h3>${(this.state.quantity * totale)}.00</h3> {/*ici on affecte le total (produit+quantité+option) c'est pas de tout il y'a grand total qui englobe la somme de la commande */}
              </div> 
            </div>
            <div className={'right'} onClick={this.onAddItem}>
              <h3>Add Item</h3>
            </div>
          </div>
        </div>
      </div>
    )
        
  }
}

export default ProductDetailModal
