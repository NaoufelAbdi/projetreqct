export default interface Option {
  name: string
  price: number
}
