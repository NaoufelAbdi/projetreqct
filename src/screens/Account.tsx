import React from 'react'
import { connect } from 'react-redux'
import OrderList from '../components/OrderList'
import Page from '../components/Page'
import AccountHeader from '../components/AccountHeader'
import HorizontalLine from '../components/HorizontalLine'
import AccountOptionsData from '../mock/AccountOptions'
import OrderHistory from '../components/OrderHistory'
import ChangePassword from '../components/ChangePassword'
import PersonalDetails from '../components/PersonalDetails'
import { logoutUser } from '../redux/user/actions'
import {LoadOrders} from '../redux/orders/actions'
import { User } from '../redux/user/types'
import ProductDetailModal from '../components/modals/ProductDetailModal'
import { Product } from '../redux/products/types'
import { ProductExtraIngredientsOptions, ProductIngredientsOptions } from '../mock/ProductOptionsData'
import Option from '../interfaces/Option'
import SizeOptionsData from '../mock/SizeOptionsData'
import { withTranslation, WithTranslation } from "react-i18next";
import { CartItem } from '../redux/cart/types'
import { Redirect } from 'react-router-dom'
import OrderDetailModel from '../components/modals/OrderDetailModal'
import PasswordChangeModel from '../components/modals/ChangePasswordModel'
import UpdateModel from '../components/modals/UpdateModel'

interface State {
  cartSlideVisible: boolean
  categorySlideVisible: boolean
  selectedSetting: number;
  oldPasswordCheck: boolean;
  productDetailModalVisible: boolean;
  selectedProduct?: Product
  options: Option[]
  extraOptions: Option[]
  selectedOptions: Option[]
  selectedExtraOptions: Option[]
  sizes: Option[]
  selectedSize: Option
  userChanged: Boolean
  orderDetailsModelVisible: Boolean
  PasswordChangeModel: Boolean
  UserUpdateModelVisible: Boolean
  UserUpdateCancel: Boolean
  passwordModelVisible: Boolean
}

interface Props extends WithTranslation {
  logoutUser?(): void
  loadOrders?(): void
  user?: User
  auth?: any
  history?: any;
}
class Account extends React.Component<Props, State> {
  state = {
    cartSlideVisible: false,
    categorySlideVisible: false,
    selectedSetting: 0,
    oldPasswordCheck: false,
    productDetailModalVisible: false,
    selectedProduct: undefined,
    options: ProductIngredientsOptions as Option[],
    extraOptions: ProductExtraIngredientsOptions as Option[],
    selectedOptions: ProductIngredientsOptions as Option[],
    selectedExtraOptions: [] as Option[],
    sizes: SizeOptionsData as Option[],
    selectedSize: SizeOptionsData[0] as Option,
    userChanged: false,
    orderDetailsModelVisible: true,
    passwordModelVisible: false,
    PasswordChangeModel: false,
    UserUpdateModelVisible: false,
    UserUpdateCancel: true
  }

  componentDidMount = (): void => {
    this.props.loadOrders()
  }
  componentWillReceiveProps(props: any) {
    if (props.auth.logOutStatus && this.props.auth.logOutStatus !== props.auth.logOutStatus) {
      this.setState({ userChanged: true });
    }
    if(props.auth.changePassword && this.props.auth.changePassword !== props.auth.changePassword){
      this.setState({passwordModelVisible: true})
      setTimeout(()=>{this.setState({passwordModelVisible: false , oldPasswordCheck: false})},1000);
    }
    if(props.auth.updateUser && this.props.auth.updateUser !== props.auth.updateUser){
      this.setState({UserUpdateModelVisible: true})
      setTimeout(()=>{this.setState({UserUpdateModelVisible: false, UserUpdateCancel: false })},1000);
    }
  }
  onCartSlideToggle = (): void => {
    this.setState({ cartSlideVisible: !this.state.cartSlideVisible })
  }
  onSettingPress = (setting: number): void => {
    this.setState({ selectedSetting: setting })

  }

  onLogoutUser = () => {
    if (this.props.logoutUser) {
      this.props.logoutUser()
    }
  }

  onNextPress = () => {
    this.setState({ oldPasswordCheck: true })
  }

  onReceiptClick = (product: Product): void => {
    this.setState({ productDetailModalVisible: true, selectedProduct: product })
  }

  onSizePress = (size: Option): void => {
    this.setState({ selectedSize: size })
  }

  closeProductDetailToggle = (): void => {
    this.setState({ productDetailModalVisible: false, selectedProduct: undefined })
  }

  onUpdateItem = (cartItem: CartItem): void => {
    this.closeProductDetailToggle()
    if (cartItem.quantity > 0) {
      // @ts-ignore
      this.props.addProductToCart(cartItem)
    }
  }

  onIngredientToggle = (option: Option): void => {
    this.setState({ selectedOptions: this.toggleOptionInOptionsArray(option, this.state.selectedOptions) })
  }

  onExtraIngredientToggle = (option: Option): void => {
    this.setState({ selectedExtraOptions: this.toggleOptionInOptionsArray(option, this.state.selectedExtraOptions) })
  }

  toggleOptionInOptionsArray = (option: Option, options: Option[]): Option[] => {
    const index: number = options.indexOf(option)
    if (index > -1) {
      options.splice(index, 1)
    } else {
      options.push(option)
    }
    return [...options]
  }
  handleOpenDetails = () => {
    this.setState({ orderDetailsModelVisible: true })
  }
  handleOnClose = () => {
    this.setState({ orderDetailsModelVisible: false })
  }
  closePasswordToggle = (): void => {
    this.setState({ passwordModelVisible: false })
  }
  render() {
    const { t } = this.props
    if(!this.props.auth.userInfo){
      return (<Redirect to={"/"} />)
    }
    if (this.state.userChanged) {
      return (<Redirect to={"/"} />);
    }
    const { userInfo } = this.props.auth
    return (
      <Page user={this.props.user}>
        <AccountHeader onCartToggle={this.onCartSlideToggle} />
        <section id={'account'}>
          <div className={"acountPageDiv"}>
            <div className={'left-card'}>
              <div className={'card welcome'}>
                <h6>{t('Welcome Back')}</h6>
                <h3>{userInfo.firstName ? userInfo.firstName : "_"}</h3>
                <p>{t('This is your dashboard!')}</p>
              </div>
              <div className={'bottom-left'}>
                <div className={'card options'}>
                  {AccountOptionsData.map((option, index) => (
                    <div>
                      <button onClick={() => {
                        index === 3 ?
                          this.onLogoutUser() :
                          this.onSettingPress(index)
                      }}>
                        <h4 className={index === this.state.selectedSetting ? 'active' : ''}>
                          {t(`AccountData.${index}.name`)}
                        </h4>
                        <div className={"iconsDivAccount"}>
                          <option.icons style={{width: "30px" , height:"30px" , padding:"6px"}} className={index === this.state.selectedSetting ? 'active' : ''} />
                        </div>
                        {/*<img className={index === this.state.selectedSetting ? 'active' : ''} src={option.img} />*/}

                      </button>
                      {index < AccountOptionsData.length - 1 && (<HorizontalLine />)}
                    </div>
                  ))}
                </div>
                {this.state.selectedSetting === 0 && (
                  <OrderHistory orderDetails={this.handleOpenDetails} />
                )}
                {this.state.selectedSetting === 1 && (
                  <ChangePassword onNext={this.onNextPress} oldPassCheck={this.state.oldPasswordCheck} />
                )}
                {this.state.selectedSetting === 2 && (
                  <PersonalDetails onChangeUpdate={this.state.UserUpdateCancel} />
                )}
                {this.state.selectedSetting === 3 && (
                  <div className={'card detail'} />
                )}
              </div>
            </div>
          </div>

          <div className={"acount-part-orders"}>
            <OrderList slideVisible={this.state.cartSlideVisible} onSlideToggle={this.onCartSlideToggle}
              onReceiptClick={this.onReceiptClick} />
          </div>
          <OrderDetailModel onClose={this.handleOnClose} visible={this.state.orderDetailsModelVisible}
          />
          <PasswordChangeModel visible={this.state.passwordModelVisible}  close={this.closePasswordToggle}/>
          <UpdateModel visible={this.state.UserUpdateModelVisible}  close={this.closePasswordToggle}/>
          <ProductDetailModal
            product={this.state.selectedProduct}
            sizesData={this.state.sizes}
            onSizePress={this.onSizePress}
            selectedSize={this.state.selectedSize}
            selectedOptions={this.state.selectedOptions}
            selectedExtraOptions={this.state.selectedExtraOptions}
            onOptionPress={this.onIngredientToggle}
            onExtraIngredientPress={this.onExtraIngredientToggle}
            onAddItem={this.onUpdateItem}
            onClose={this.closeProductDetailToggle} visible={this.state.productDetailModalVisible}
          />
        </section>
      </Page>
    )
  }
}

const comp = connect((state: any) => {
  return {
    user: state.auth,
    auth: state.auth,
  };
}, (dispatch: any) => {
  return {
    logoutUser: () => dispatch(logoutUser()),
    loadOrders: () => dispatch(LoadOrders()),
  };
})(Account);

export default withTranslation()(comp);
